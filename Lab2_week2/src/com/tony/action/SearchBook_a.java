package com.tony.action;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import com.tony.db.DBConnection;
import com.tony.model.Book;


public class SearchBook_a extends ActionSupport{

	private String genre;
	private String title;
	private String author;
	private boolean fuzzySearch;
	
	private List<Book> bookList = new LinkedList<Book>();
	
	
	public SearchBook_a(){}
	
	public String execute(){
		String rtn = null;
		
		System.out.println("genre="+genre);
		System.out.println("title="+title);
		System.out.println("author="+author);
		System.out.println("fuzzySearch="+fuzzySearch);
		
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try{
			
			StringBuffer sqlStrSb = new StringBuffer("SELECT * FROM BOOK WHERE");
			
			if (!this.fuzzySearch) {
				if (!StringUtils.isBlank(this.genre)) {
					sqlStrSb.append(" Genre='" + this.genre + "' AND");
				}

				if (!StringUtils.isBlank(this.author)) {
					sqlStrSb.append(" Author='" + this.author + "' AND");
				}

				if (!StringUtils.isBlank(this.title)) {
					sqlStrSb.append(" Title='" + this.title + "' AND");
				}
			}else{
				if (!StringUtils.isBlank(this.genre)) {
					sqlStrSb.append(" Genre LIKE '%" + this.genre + "%' AND");
				}

				if (!StringUtils.isBlank(this.author)) {
					sqlStrSb.append(" Author LIKE '%" + this.genre + "%' AND");
				}

				if (!StringUtils.isBlank(this.title)) {
					sqlStrSb.append(" Title LIKE '%" + this.genre + "%' AND");
				}
			}
			
			
			String sqlStr =  sqlStrSb.substring(0, sqlStrSb.length()-3);
			System.out.println(sqlStr);
			conn = DBConnection.createConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sqlStr);
			
			this.bookList.clear();
			while(rs.next()){
				Book book = new Book();
				
				book.setBookId(rs.getInt(Book.BOOK_ID));
				book.setBlurb(rs.getString(Book.BLURB));
				book.setAuthor(rs.getString(Book.AUTHOR));
				book.setGenre(rs.getString(Book.GENRE));
				book.setIsbn(rs.getString(Book.ISBN));
				book.setTitle(rs.getString(Book.TITLE));
				
				this.bookList.add(book);
				
			}

			System.out.println(this.bookList.size());
			
			rtn = Action.SUCCESS;
		}catch(SQLException sqle){
			sqle.printStackTrace();
			rtn = Action.ERROR;
		}finally{
			if(rs!=null)
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if(stmt!=null)
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				
			/*
			if(conn!=null)
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			*/
		}
		
		return rtn;
	}
	
	/**
	 * @return the genre
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * @param genre the genre to set
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the fuzzySearch
	 */
	public boolean getFuzzySearch() {
		return fuzzySearch;
	}

	/**
	 * @param fuzzySearch the fuzzySearch to set
	 */
	public void setFuzzySearch(boolean fuzzySearch) {
		this.fuzzySearch = fuzzySearch;
	}

	/**
	 * @return the bookList
	 */
	public List<Book> getBookList() {
		return bookList;
	}

	/**
	 * @param bookList the bookList to set
	 */
	public void setBookList(List<Book> bookList) {
		this.bookList = bookList;
	}
	

	@Override
	public void validate() {
	//	System.out.println("validate. isPostBack="+this.isPostBack);
	//	System.out.println("actionError<"+this.getActionErrors().size()+">");
		
		
		if(StringUtils.isBlank(this.title)&&StringUtils.isBlank(this.author)&&StringUtils.isBlank(this.genre)){
			this.addActionError("At lest one criterion is needed.");
//			this.addFieldError("genre", "I don't want to be here.");
//			if(StringUtils.isBlank(this.title))
//				this.addFieldError("title", "");
		}
//		System.out.println("actionError<"+this.getActionErrors().size()+">");
		super.validate();
		
	//	System.out.println(INPUT);
	}
}
