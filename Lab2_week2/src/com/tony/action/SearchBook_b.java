package com.tony.action;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import com.tony.db.DBConnection;
import com.tony.model.Book;


public class SearchBook_b{

	private String genre;
	private String title;
	private String author;
	
	private List<Book> bookList = new LinkedList<Book>();
	
	private List<String> genreList;
	
	public SearchBook_b(){
		Connection conn = DBConnection.createConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
		
			this.genreList = new LinkedList<String>();
			
			rs= stmt.executeQuery("SELECT DISTINCT "+Book.GENRE+" FROM book");
			System.out.println("SELECT DISTINCT "+Book.GENRE+" FROM book    "+rs.getMetaData().getColumnCount());
			while(rs.next()){
				((LinkedList<String>)this.genreList).addLast(rs.getString(1));
			}
		
			System.out.println(this.genreList.size());
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(stmt!=null)
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if(rs!=null)
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		
		
	}
	
	public String execute(){
		String rtn = Action.SUCCESS;
		
		System.out.println("genre="+genre);
		System.out.println("title="+title);
		System.out.println("author="+author);
		
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try{
			
			StringBuffer sqlStrSb = new StringBuffer("SELECT * FROM BOOK WHERE");

			if (StringUtils.isNotBlank(this.genre) && !this.genre.equalsIgnoreCase("-1")) {
				sqlStrSb.append(" Genre='" + this.genre + "' AND");
			}

			if (!StringUtils.isBlank(this.author)) {
				sqlStrSb.append(" Author='" + this.author + "' AND");
			}

			if (!StringUtils.isBlank(this.title)) {
				sqlStrSb.append(" Title='" + this.title + "' AND");
			}
			
			String sqlStr;
			if (sqlStrSb.toString().endsWith("WHERE")){
				sqlStr =  sqlStrSb.substring(0, sqlStrSb.length()-5);
			}else{
				sqlStr =  sqlStrSb.substring(0, sqlStrSb.length()-3);
			}
			
			System.out.println(sqlStr);
			conn = DBConnection.createConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sqlStr);
			
			this.bookList.clear();
			while(rs.next()){
				Book book = new Book();
				
				book.setBookId(rs.getInt(Book.BOOK_ID));
				book.setBlurb(rs.getString(Book.BLURB));
				book.setAuthor(rs.getString(Book.AUTHOR));
				book.setGenre(rs.getString(Book.GENRE));
				book.setIsbn(rs.getString(Book.ISBN));
				book.setTitle(rs.getString(Book.TITLE));
				
				this.bookList.add(book);
				
			}

			System.out.println(this.bookList.size());
			
			rtn = Action.SUCCESS;
		}catch(SQLException sqle){
			sqle.printStackTrace();
			rtn = Action.ERROR;
		}finally{
			if(rs!=null)
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if(stmt!=null)
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				
			/*
			if(conn!=null)
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			*/
		}
		
		return rtn;
	}
	
	/**
	 * @return the genre
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * @param genre the genre to set
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the bookList
	 */
	public List<Book> getBookList() {
		return bookList;
	}

	/**
	 * @param bookList the bookList to set
	 */
	public void setBookList(List<Book> bookList) {
		this.bookList = bookList;
	}

	/**
	 * @return the genreList
	 */
	public List<String> getGenreList() {
		return genreList;
	}

	/**
	 * @param genreList the genreList to set
	 */
	public void setGenreList(List<String> genreList) {
		this.genreList = genreList;
	}
	
}
