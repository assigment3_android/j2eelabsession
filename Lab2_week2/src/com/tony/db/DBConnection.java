package com.tony.db;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;


public class DBConnection {
	private Connection conn = null;
	
	private static DBConnection instance;
	
	private DBConnection(){
		super();
	}
	
	
	public static Connection createConnection(){
		
		if (instance==null) instance = new DBConnection();
		
		try {
			if (instance.conn!=null&&(!instance.conn.isClosed())) return instance.conn;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/*
		 * ==============================================================================================
		 * =========try to connect to the local mysql server============================================= 
		 * ============================================================================================== 
		 */
		try{// 
			MysqlDataSource ds = new MysqlDataSource();
			ds.setURL("jdbc:mysql://localhost/j2eelab2");
			ds.setPassword("j2ee");
			ds.setUser("jee");
			
			System.out.println("trying to connected jdbc:mysql://localhost/jee...");
			instance.conn = ds.getConnection();
			
			System.out.println("connected!");
			
		}catch (SQLException e){
			e.printStackTrace();
			System.out.println("fail to connect to Mysql");
		}
		
		
		return instance.conn;
	}

	@Override
	protected void finalize() throws Throwable {
		if(this.conn!=null) this.conn.close();
		super.finalize();
	}
	
	
}
