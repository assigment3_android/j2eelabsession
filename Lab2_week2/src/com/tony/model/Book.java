package com.tony.model;

public class Book {

	//field names in database;
	public static final String BOOK_ID = "BOOK_ID";
	public static final String TITLE = "TITLE";
	public static final String AUTHOR = "AUTHOR";
	public static final String GENRE = "GENRE";
	public static final String ISBN = "ISBN";
	public static final String BLURB = "BLURB";

	public Book() {
	}

	private int bookId;
	private String title;
	private String author;
	private String genre;
	private String isbn;
	private String blurb;

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getBlurb() {
		return blurb;
	}

	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}
}
