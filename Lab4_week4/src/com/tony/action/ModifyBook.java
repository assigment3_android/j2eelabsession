package com.tony.action;

import com.tony.controller.BookManager;
import com.tony.model.Book;
import com.tony.model.Genre;
import com.opensymphony.xwork2.Action;

import java.util.HashSet;
import java.util.Set;


public class ModifyBook
{	
	private BookManager linkController;
	
	private Long bookId;
	private String title;
	private String author;
	private String genre;
	private String isbn;
	private String blurb;
	
	public ModifyBook()
	{
		linkController = new BookManager();
	}
	
	public String add()
	{
		Book book = new Book();
		
		book.setAuthor(author);
		book.setTitle(title);
		book.setBlurb(blurb);
		
		book.setIsbn(isbn);
		
		linkController.add(book, genre);
		
		return Action.SUCCESS;
	}
	
	public String update()
	{
		// get the current book
		
		Book book = linkController.get(getBookId());
		
		// update the book setters based on new form data
		book.setAuthor(author);
		book.setTitle(title);
		book.setIsbn(isbn);
		
		book.setBlurb(blurb);
		
		// actually update the book using the linkController
		linkController.update(book, genre);
		
		return "success";
	}
	
	public String execute()
	{
		Book book = linkController.get(getBookId());
		
		if(book !=  null){
			this.author = book.getAuthor();
			this.title = book.getTitle();
			
			//code to separate the different genre records
			if(book.getGenres().size()>0){
				StringBuffer genreSb = new StringBuffer();
				for (Genre genre : book.getGenres()){
					genreSb.append(genre.getDescription()+",");
				}
			
				this.genre = genreSb.substring(0, genreSb.length()-1);
			}else{
				this.genre = "";
			}
			
			this.blurb = book.getBlurb();
			this.isbn = book.getIsbn();
			
			return "update";
		}
		else
		{
			return "add";
		}
		
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getBlurb() {
		return blurb;
	}

	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}
}
