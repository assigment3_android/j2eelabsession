package com.tony.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.tony.controller.BookManager;
import com.tony.model.Book;
import com.opensymphony.xwork2.Action;

public class SearchBook
{	
	private BookManager linkController;
	
	public SearchBook()
	{
		linkController = new BookManager();
		books = new HashSet<Book>();
	}
	
	private String title;
	private String author;
	private Long bookId;
	
	Set<Book> books;

	public String delete()
	{
		System.out.println(this.getBookId());
		linkController.delete(getBookId());
		return Action.SUCCESS;
	}
	
	public String query()
	{
		//TODO get a list of books back from our linkController
	//	books.clear();
		
		books.addAll(linkController.list(title, author));
		
		return Action.SUCCESS;
	}
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void Set(Set<Book> books) {
		this.books = books;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
}
