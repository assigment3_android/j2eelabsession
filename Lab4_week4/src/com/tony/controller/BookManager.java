package com.tony.controller;


import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.tony.model.Book;
import com.tony.model.Genre;
import com.tony.util.HibernateUtil;

public class BookManager extends HibernateUtil {

	public Book update(Book book, String genreStr) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try{
			Book book4Update = (Book)session.load(Book.class, book.getBookId()); 
			book4Update.setAuthor(book.getAuthor());
			book4Update.setBlurb(book.getBlurb());
			book4Update.setIsbn(book.getIsbn());
			book4Update.setTitle(book.getTitle());
			
			Set<Genre> genres = new HashSet<Genre>();
				
			
			//code to combine different genre records using commas when displaying the update user interface
			genreStr = genreStr.trim();
			if(genreStr.startsWith(",")) 
				genreStr = genreStr.substring(1);
			
			if(genreStr.endsWith(",")) 	
				genreStr = genreStr.substring(1, genreStr.length()-1);
			
			String[] genreStrings = genreStr.split(",");
			for (String genreString : genreStrings) {
				// look for exist genre
				Genre genre = null;
				Criteria criteria = session.createCriteria(Genre.class);
				criteria.add(Restrictions.eq("description", genreString.trim()));
				List genreList = criteria.list();
				if (genreList.size() > 0) {
					genre = (Genre) genreList.get(0);
				}

				if (genre == null) {
					genre = new Genre();
					genre.setDescription(genreString);
				}

				genres.add(genre);
			}

			
			book4Update.setGenres(genres);
			session.update(book4Update);
			
			book = book4Update;
			
			session.getTransaction().commit();
		}catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		
		return book;
	}
	
	
	
	public Book add(Book book, String genreStr) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try{
			Set<Genre> genres = new HashSet<Genre>();
					
			//code to combine different genre records using commas when displaying the update user interface
			genreStr = genreStr.trim();
			if(genreStr.startsWith(",")) 
				genreStr = genreStr.substring(1);
			
			if(genreStr.endsWith(",")) 	
				genreStr = genreStr.substring(1, genreStr.length()-1);
			
			String[] genreStrings = genreStr.split(",");
			for (String genreString : genreStrings) {
				// look for exist genre
				Genre genre = null;
				Criteria criteria = session.createCriteria(Genre.class);
				criteria.add(Restrictions.eq("description", genreString.trim()));
				List genreList = criteria.list();
				if (genreList.size() > 0) {
					genre = (Genre) genreList.get(0);
				}

				if (genre == null) {
					genre = new Genre();
					genre.setDescription(genreString);
				}

				genres.add(genre);
			}

			
			book.setGenres(genres);
			session.save(book);
			
			session.getTransaction().commit();
		}catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		
		return book;
	}
	
	public Book delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		Book book = (Book) session.load(Book.class, id);
		
		if(null != book) {
			session.delete(book);
		}
		
		session.getTransaction().commit();
		return book;
	}

	public Book get(Long id) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		LinkedList<Book> books = new LinkedList<Book>();
		try {
			Criteria criteria = session.createCriteria(Book.class);
			criteria.add(Restrictions.eq("bookId", id));
			
			books.addAll((List<Book>)criteria.list());
			
		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		session.getTransaction().commit();
		
		if(!books.isEmpty())
		{
			return books.getFirst();
		}
		else
		{
			return null;
		}
	}

	public Set<Book> list(String title, String author) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Set<Book> books = new HashSet<Book>();;
		try {
			
			Criteria criteria = session.createCriteria(Book.class);
			
			
			if(StringUtils.isNotBlank(title)){
				criteria.add(Restrictions.like("title", "%"+title+"%"));
			}
			
			if(StringUtils.isNotBlank(author)){
				criteria.add(Restrictions.like("author", "%"+author+"%"));
			}
			
			
			for (Book book :(List<Book>)criteria.list()){// get the list
				books.add(book);
			}
			
			System.out.println("==="+books.size());
			// do the same thing using HQL
			
	//		Query query = session.createQuery("from Book b where b.genre = :genre");
	//		query.setString("genre", genre);
	
		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
			
		session.getTransaction().commit();
			
		return books;
	}

	public Genre getGenre(String genreString) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Genre genre = null;
		try{
			Criteria criteria = session.createCriteria(Genre.class);
			
			criteria.add(Restrictions.eq("description", genreString.trim()));
			
			List genreList = criteria.list();
			if (genreList.size()>0){
				genre = (Genre)genreList.get(0);
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
			
		session.getTransaction().commit();
		
		return genre;
	}




}
