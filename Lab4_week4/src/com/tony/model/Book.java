package com.tony.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Book")
public class Book implements Serializable {
	private static long serialVersionUID = -6837174276610847586L;
	private Long bookId;
	private String title;
	private String author;
	private String isbn;
	private String blurb;
	
	private Set<Genre> genres;

	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column(name="book_id")
	public Long getBookId() {
		return bookId;
	}
	@Column(name="title")
	public String getTitle() {
		return title;
	}
	@Column(name="author")
	public String getAuthor() {
		return author;
	}
	@Column(name="isbn")
	public String getIsbn() {
		return isbn;
	}
	@Column(name="blurb")
	public String getBlurb() {
		return blurb;
	}
	
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}
	
	@ManyToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(name="BookGenre", joinColumns={@JoinColumn(name="bookId")}, inverseJoinColumns={@JoinColumn(name="genreId")})
	/**
	 * @return the bookGenres
	 */
	public Set<Genre> getGenres() {
		return this.genres;
	}
	/**
	 * @param bookGenres the bookGenres to set
	 */
	public void setGenres(Set<Genre> genres) {
		this.genres = genres;
		/*
		if (this.bookGenres.size() == 0){
			this.genreString = "";
			return;
		}
		
		StringBuffer genreSb = new StringBuffer();
		for (BookGenre bookgenre : this.bookGenres){
			genreSb.append(bookgenre.getGenre().getDescription()+",");
		}
		
		this.genreString = genreSb.substring(0, genreSb.length()-1);
		*/
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @param serialversionuid the serialversionuid to set
	 */
	public static void setSerialversionuid(long serialversionuid) {
		serialVersionUID = serialversionuid;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		Book other = (Book)obj;
		if(this.getBookId() == other.bookId) return true;
		else return false;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}