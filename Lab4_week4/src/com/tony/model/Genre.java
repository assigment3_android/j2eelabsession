package com.tony.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="genre")
public class Genre implements Serializable {
	private long genreId;
	private String description;
	private List<Book> books;
	
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column (name="genreId")
	/**
	 * @return the genreId
	 */
	public long getGenreId() {
		return genreId;
	}
	/**
	 * @param genreId the genreId to set
	 */
	public void setGenreId(long genreId) {
		this.genreId = genreId;
	}
	
	@Column (name="description")
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	@ManyToMany (fetch= FetchType.EAGER)
	@JoinTable(name="BookGenre", joinColumns={@JoinColumn(name="genreId")}, inverseJoinColumns={@JoinColumn(name="bookId")})
	
	public List<Book> getBooks() {
		return this.books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.description;
	}
	
	
}
