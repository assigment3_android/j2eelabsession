package com.tony;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class Client extends JFrame implements ActionListener, Runnable, ListSelectionListener, WindowListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTextField loginNameTf = new JTextField(8); 
	JButton loginBtn = new JButton("Login");
	
	JTextArea broadcastTa = new JTextArea(5,30);
	
	DefaultListModel<String> userDml = new DefaultListModel<String>();
	JList<String> userLst = new JList<String>();
	
	JTextArea chatMsgTa = new JTextArea(5,20);
	JTextField chatInputTf = new JTextField(20);
	JButton chatMsgBtn = new JButton("Send");
	
	Socket client;
	DataInputStream dis;
	DataOutputStream dos;
	String chatUserName;
	String loginName;
	
	
	public Client(){
		Container contentPanel = this.getContentPane();
		contentPanel.setLayout(new BorderLayout());
		
		//=======================================================================
		JPanel westPanel = new JPanel();
		westPanel.setLayout(new BorderLayout());
		westPanel.add(new JLabel("BROADCAST"), BorderLayout.NORTH);
		westPanel.add(new JScrollPane(this.broadcastTa), BorderLayout.CENTER);
		westPanel.setBackground(Color.gray);
		contentPanel.add(westPanel, BorderLayout.WEST);
		//========================================================================
		
		
		//========================================================================
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		centerPanel.setSize(50, 600);
		centerPanel.add(new JLabel("Users"), BorderLayout.NORTH);
		
		userDml.addElement("ALL USERS");
		userLst.setModel(userDml);
		userLst.setSelectedIndex(0);
		chatUserName = "ALL USERS";
		userLst.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		userLst.addListSelectionListener(this);
		
		centerPanel.add(new JScrollPane(this.userLst), BorderLayout.CENTER);
		centerPanel.setBackground(Color.gray);
		contentPanel.add(centerPanel, BorderLayout.CENTER);
		//========================================================================
		
		//========================================================================
		JPanel eastPanel = new JPanel();
		eastPanel.setBackground(Color.red);
		eastPanel.setLayout(new BorderLayout());
		
		JPanel eastPanel1 = new JPanel();
		eastPanel1.setLayout(new BorderLayout());
		eastPanel1.add(this.loginNameTf, BorderLayout.CENTER);
		eastPanel1.add(this.loginBtn, BorderLayout.EAST);
		loginBtn.addActionListener(this);
		chatMsgBtn.addActionListener(this);
		eastPanel.add(eastPanel1, BorderLayout.NORTH);
//		
		JPanel eastPanel2 = new JPanel();
		eastPanel2.setLayout(new BorderLayout());
		eastPanel2.add(this.chatMsgTa, BorderLayout.CENTER);
		eastPanel.add(new JScrollPane(this.chatMsgTa), BorderLayout.CENTER);
//		
		JPanel eastPanel3 = new JPanel();
		eastPanel3.setLayout(new BorderLayout());
		eastPanel3.add(this.chatInputTf, BorderLayout.CENTER);
		eastPanel3.add(this.chatMsgBtn, BorderLayout.EAST);
		eastPanel.add(eastPanel3, BorderLayout.SOUTH);
//		
		contentPanel.add(eastPanel, BorderLayout.EAST);

		this.addWindowListener(this);
		
		pack();
		this.setSize(800, 300);
		setVisible(true);
		
//		chatMessageBtn.addActionListener(this);
		
//		try {
//			
//			client = new Socket("localhost",4999);
//			dis = new DataInputStream(client.getInputStream());
//			dos = new DataOutputStream(client.getOutputStream());
//			
//			Thread clientThread = new Thread(this);
//			clientThread.start();
//		}
//		catch (UnknownHostException e) {
//			e.printStackTrace();
//		}
//		catch (IOException e) {
//			e.printStackTrace();
//		}
	}
	
	public void login(String userName) throws IOException{
		dos.writeUTF(ServerConstants.LOGIN+userName);
	}
	
	public void logout() throws IOException{
		dos.writeUTF(ServerConstants.LOGOUT+this.loginName);
		
		this.loginName = null;
	}
	
	public void sendMsg(String msg) throws IOException{
		
		byte msgType = -1;
		if(this.chatUserName.equals("ALL USERS")){
			msgType = ServerConstants.CHAT_BROADCAST;
			msg = msgType+msg;
		}else{
			msgType = ServerConstants.CHAT_MESSAGE;
			msg = (msgType+this.chatUserName+","+msg);
		}
		
		System.out.println("send chat msg==>"+msg);
		dos.writeUTF(msg);
		
		dos.flush();
		
		this.chatInputTf.setText("");
	}
	
	public void sendBroadcast(String msg){
		
	}
	
	
	public static void main(String[] args){
		Client client = new Client();
	}

	@Override
	public void actionPerformed(ActionEvent event){
		JButton button = (JButton)event.getSource();
		
		String s = button.getText();
			switch (s){
			
			case ("Login"):
				//validate login name
				String aLoginName = this.loginNameTf.getText().trim();
				if (aLoginName.length()==0){
					this.chatMsgTa.append("Login Name must be provided.\r\n");
					break;
				}else if (aLoginName.indexOf(",")!=-1){
					this.chatMsgTa.append("Login Name must not contain comma.\r\n");
					break;
				}
				
				
			
				try {
					if(this.loginName!=null){
						this.logout();
					}
					
					client = new Socket("localhost",ServerConstants.PORT);
					dis = new DataInputStream(client.getInputStream());
					dos = new DataOutputStream(client.getOutputStream());
				
					Thread clientThread = new Thread(this);
					clientThread.start();
	
					//send login 
					this.login(aLoginName);
					
				}catch (UnknownHostException e) {
					e.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
				
				break;
				
			case ("Send") :
			
				if(this.loginName==null){
					this.chatMsgTa.append("Please login.\r\n");
					return;
				}
				
				if(this.chatUserName==null){
					this.chatMsgTa.append("Please select a user to send the message to.\r\n");
					return;
				}
				
				try {
					this.sendMsg(this.chatInputTf.getText());
					
				}
				catch (IOException e){
					e.printStackTrace();
				}
			
				break;
			
		}
	}

	@Override
	public void run(){
		while(true){
			try {
				String msg = dis.readUTF();
				
				System.out.println("Received: "+msg);
				
				int msgType = Byte.parseByte(msg.substring(0,1));
				String msgBody = null;
				
				switch(msgType){
				case (ServerConstants.LOGIN_SUCCESS):
					msgBody = msg.substring(1);
					this.loginName = msgBody;
					this.chatMsgTa.append(msgBody+" login successful.\r\n");
				
				case ServerConstants.USER_LIST:
					msgBody = msg.substring(1, msg.length());
					String[] usersArray = msgBody.split(",");
					
					this.userDml.clear();
					this.userDml.addElement("ALL USERS");
					for(String userName : usersArray){
						if(userName.equals(this.loginName)) continue;
						
						this.userDml.addElement(userName);
					//	System.out.println(userName);
					}
					this.userLst.setModel(userDml);
					
					break;
				
				case (ServerConstants.CHAT_BROADCAST):
					msgBody = msg.substring(1, msg.length());
					this.broadcastTa.append(msgBody+"\r\n");
					break;
					
				case (ServerConstants.CHAT_MESSAGE):
					msgBody = msg.substring(1, msg.length());
					this.chatMsgTa.append(msgBody+"\r\n");
				}
				
				
			}
			catch (IOException e)
			{
				e.printStackTrace();
				break;
			}
			
		}
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		JList list = (JList) e.getSource();
		String selectedValue = (String) list.getSelectedValue();
		this.chatUserName = selectedValue;
		System.out.println(this.chatUserName);
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		System.exit(0);
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		try {
			this.logout();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
