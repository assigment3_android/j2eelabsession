package com.tony;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.ScrollPane;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Server extends JFrame implements ActionListener, WindowListener{
	
	private ServerSocket serverSocket;
	private Map<String, ServerThread> clientMap; 
	
	JTextArea systemMsgTa = new JTextArea(10,20);
	JButton exitBtn = new JButton("Close Server");
	
	private Server(){
		Container contentPanel = this.getContentPane();
		contentPanel.setLayout(new BorderLayout());
		contentPanel.add(new JScrollPane(systemMsgTa), BorderLayout.CENTER);
		this.exitBtn.addActionListener(this);
		contentPanel.add(this.exitBtn, BorderLayout.SOUTH);
		
		this.addWindowListener(this);
		
		pack();
		this.setSize(320, 600);
		setVisible(true);
		
	}

	public void startServer() throws IOException{
		this.serverSocket = new ServerSocket(ServerConstants.PORT);
		this.clientMap = new HashMap<String, ServerThread>();	
	}
	public void printSysMsg(String msg){
		this.systemMsgTa.append(msg+"\r\n");
	}
	
	public static void main(String[] args){
		Server server = new Server();
		try {
			
			server.startServer();
		//	System.out.println();
			server.printSysMsg("Server is started");
			while(true){
				server.printSysMsg("Waiting...");
				Socket client = server.serverSocket.accept();
				server.printSysMsg("accepted!");
				
				new Thread(new ServerThread(client, server)).start();;
			}
		} catch (IOException e) {
			server.printSysMsg("Fail to start server");
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	private static class ServerThread implements Runnable{
		private DataOutputStream dos;
		private DataInputStream dis;
		private String userHandle;
		private Socket socket;
		private Server server;
		private  ServerThread(Socket socket, Server server){
			this.socket = socket;
			this.server = server;
		}
		
		private void sendMsg(String msg){
			try {
				this.dos.writeUTF(msg);
				this.dos.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		@Override
		public void run() {
			try {
				this.dis = new DataInputStream(this.socket.getInputStream());	
				this.dos = new DataOutputStream(this.socket.getOutputStream());
				
				//flag that indicates if the loop should finish 
				boolean isExit = false;
				while(!isExit){
					String msg = dis.readUTF();
					
					//msg type
					byte msgType = -1;
					String msgBody = null;
					try{
						msgType = Byte.parseByte(msg.substring(0,1)); 
						msgBody = msg.substring(1);
					}catch (Exception e){
						e.printStackTrace();
						continue;
					}
					
					server.printSysMsg("<"+msgType+":"+msgBody+">");
					
					switch(msgType){
					case (ServerConstants.LOGIN): //login, obtain the user handle.
						this.userHandle = msgBody;
						if (this.server.clientMap.get(this.userHandle)!=null){
							this.sendMsg(ServerConstants.LOGIN_FAIL+this.userHandle);
							
							this.dis.close();
							this.dos.close();
							this.socket.close();
							
							server.printSysMsg("Name : "+ this.userHandle+" has been used,connection is closed.");
							
						}else{
							this.server.clientMap.put(this.userHandle, this);
							this.sendMsg(ServerConstants.LOGIN_SUCCESS + this.userHandle);
							
							this.broadcastUserList();
							
							server.printSysMsg("Name : "+ this.userHandle+" login successfully.");
						}
						
						break;
					case (ServerConstants.LOGOUT): //login, obtain the user handle. 
						this.server.clientMap.remove(this.userHandle).socket.close();
	
						this.broadcastUserList();
						
						break;
					
					case (ServerConstants.CHAT_BROADCAST): // broadcast msg
						
						for (ServerThread thread : this.server.clientMap.values()){
							thread.sendMsg(ServerConstants.CHAT_BROADCAST+"Broadcast msg from "+ this.userHandle +":"+msgBody);
						}
						server.printSysMsg("Broadcast msg from"+ this.userHandle +":"+msgBody);
						break;
							
					case (ServerConstants.CHAT_MESSAGE): //send chat msg to specific user.
						int commaIndex = msgBody.indexOf(",");
						String receiver = msgBody.substring(0,commaIndex);
						msgBody = msgBody.substring(commaIndex+1);
						
						
						ServerThread receiverThread = this.server.clientMap.get(receiver);
						if(receiverThread!=null){
							receiverThread.sendMsg(ServerConstants.CHAT_MESSAGE+"Msg from"+ this.userHandle +":"+msgBody);
							server.printSysMsg("Sent:"+msgBody+" to "+receiver);
//							System.out.print("Sent msg from"+ this.userHandle +" to "+receiver +":"+msgBody);
						}else{
							this.sendMsg("Lost connection to "+ receiver);
						}
//							System.out.print("Lost connection to "+ receiver);
//						}
							
						break;
						/*
					case (ServerConstants.EXIT_MESSAGE): //logout
						
						this.dis.close();
						this.dos.close();
						this.socket.close();
					
						System.out.print("Name : "+ this.userHandle+" logout.");
						
						isExit = true;
						break;
						*/
					default:
						server.printSysMsg("Undefined msgType:"+msgType);
					}
					
				}
				
			} catch (IOException e) {
				server.printSysMsg("Lost connection from "+this.userHandle);
				server.clientMap.remove(this.userHandle);
				this.broadcastUserList();
				e.printStackTrace();
				return;
			}
			
			
		}

		private void broadcastUserList() {
			StringBuffer userListSb = new StringBuffer();
			for (String user : this.server.clientMap.keySet()){
				userListSb.append(user);
				userListSb.append(",");
			}
			
			for(ServerThread serverThread : this.server.clientMap.values()){
				serverThread.sendMsg(ServerConstants.USER_LIST+userListSb.toString().substring(0, userListSb.lastIndexOf(",")));
			}
		}
	
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(0);
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		try {
			this.serverSocket.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
}


