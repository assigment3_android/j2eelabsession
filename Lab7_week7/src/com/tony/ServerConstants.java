package com.tony;

public class ServerConstants {
	
	public static final byte LOGIN = 0;
	public static final byte LOGIN_FAIL = 1;
	public static final byte LOGIN_SUCCESS = 2;
	public static final byte LOGOUT = 3;
	
	public static final byte CHAT_MESSAGE = 4; 
	public static final byte EXIT = 5;
	public static final byte CHAT_BROADCAST = 6;
	public static final byte USER_LIST = 7;
	
	public static final int PORT = 4999;
}
