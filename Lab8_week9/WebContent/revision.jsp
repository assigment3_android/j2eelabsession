<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Revision Data</title>
</head>
<body>



<table border="1">
<tr align="center" bgcolor="#CCCCCC">
		<td>
			Book ID
		</td>
		<td>
			Title
		</td>
		<td>
			Author
		</td>
		<td>
			Genre
		</td>
		<td>
			ISBN
		</td>
		<td>
			Blurb
		</td>
		<td>
			Revision Type
		</td>
</tr>		

<s:iterator value="revisionBooks" >

	<tr>
		<td>
			<s:property value="bookId"/>
		</td>
		
		<td>
			<s:property value="title"/>
		</td>
		<td>
			<s:property value="author"/>
		</td>
		<td>
			<s:property value="genre"/>
		</td>
		<td>
			<s:property value="isbn"/>
		</td>
		<td>
			<s:property value="blurb"/>
		</td>
		<td>
			<s:property value="revisionType"/>
		</td>
	</tr>
</s:iterator>
</table>

<s:form action="query" method="get">
<s:submit value="OK"></s:submit>
</s:form>
</body>
</html>