package com.tony.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.opensymphony.xwork2.Action;
import com.tony.controller.BookManager;
import com.tony.model.Book;
import com.tony.model.BookDTO;

public class SearchBook
{	
	private BookManager linkController;
	
	public SearchBook()
	{
		linkController = new BookManager();
		books = new ArrayList<Book>();
	}
	
	private String genre;
	private String title;
	private String author;
	private Long bookId;
	
	ArrayList <Book> books;
	ArrayList <BookDTO> revisionBooks;

	public String delete()
	{
		System.out.println(this.getBookId());
		linkController.delete(getBookId());
		return Action.SUCCESS;
	}
	
	public String revision() {

		List<Object[]> bookRevisions = this.linkController.getHistoricalBook(this.bookId);

		System.out.println("bookId=" + this.bookId);
		System.out.println("bookRevisions.size()=" + bookRevisions.size());

		this.revisionBooks = new ArrayList<BookDTO>();
		for (Object[] objects : bookRevisions) {
			this.revisionBooks.add(new BookDTO(objects));
			System.out.println("objects.length=" + objects.length);
			for (int i = 0; i < objects.length; i++) {
				System.out.println("\tobject[" + i + "]"+ objects[i].toString());
			}
		}

		System.out.println(this.revisionBooks);
		return Action.SUCCESS;

	}
	
	public String query()
	{
		//TODO get a list of books back from our linkController
		books.addAll(linkController.list(genre, title, author));
		
		return Action.SUCCESS;
	}
	
	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public ArrayList<Book> getBooks() {
		return books;
	}

	public void setBooks(ArrayList<Book> books) {
		this.books = books;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	/**
	 * @return the revisionBooks
	 */
	public ArrayList<BookDTO> getRevisionBooks() {
		return revisionBooks;
	}

	/**
	 * @param revisionBooks the revisionBooks to set
	 */
	public void setRevisionBooks(ArrayList<BookDTO> revisionBooks) {
		this.revisionBooks = revisionBooks;
	}
	
	
}
