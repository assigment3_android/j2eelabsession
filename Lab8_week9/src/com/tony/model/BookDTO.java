package com.tony.model;

import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionType;

public class BookDTO {
	private long bookId;
	private String author;
	private String blurb;
	private String title;
	private String genre;
	private String isbn;
	
	private String revisionType;
	
//	private RevisionType revisionType;
//	private RevisionEntity revisionEntity;
	
	public BookDTO(Object[] objects){
		Book book = (Book)objects[0];
		this.bookId = book.getBookId();
		this.author = book.getAuthor();
		this.title = book.getTitle();
		this.blurb = book.getBlurb();
		this.genre = book.getGenre();
		this.isbn = book.getIsbn();
		
		this.revisionType = objects[2].toString();		
	}

	/**
	 * @return the bookId
	 */
	public long getBookId() {
		return bookId;
	}

	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(long bookId) {
		this.bookId = bookId;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the blurb
	 */
	public String getBlurb() {
		return blurb;
	}

	/**
	 * @param blurb the blurb to set
	 */
	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the genre
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * @param genre the genre to set
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}

	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	/**
	 * @return the revisionType
	 */
	public String getRevisionType() {
		return revisionType;
	}

	/**
	 * @param revisionType the revisionType to set
	 */
	public void setRevisionType(String revisionType) {
		this.revisionType = revisionType;
	}
	

}
